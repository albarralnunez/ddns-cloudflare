FROM resin/rpi-raspbian
MAINTAINER Daniel Albarral "albarralnunezATgmailDOTcom"

COPY ddns-cloudflare.sh /usr/bin/ddns-cloudflare

RUN chmod +x usr/bin/ddns-cloudflare

CMD ddns-cloudflare

