#!/usr/bin/env bash

if [ -z "${CLOUDFLARE_AUTH_KEY}" ]; then
    echo "Error: Missing environment variable CLOUDFLARE_AUTH_KEY."
    exit 0
fi
if [ -z "${CLOUDFLARE_AUTH_EMAIL}" ]; then
    echo "Error: Missing environment variable CLOUDFLARE_AUTH_EMAIL."
    exit 0
fi
if [ -z "${CLOUDFLARE_DOMAIN_NAME}" ]; then
    echo "Error: Missing environment variable CLOUDFLARE_DOMAIN_NAME."
    exit 0
fi
if [ -z "${CLOUDFLARE_API}" ]; then
    echo "Error: Missing environment variable CLOUDFLARE_API."
    exit 0
fi
if [ -z "${CLOUDFLARE_ZONE_ID}" ]; then
    echo "Error: Missing environment variable CLOUDFLARE_ZONE_ID."
    exit 0
fi
if [ -z "${CLOUDFLARE_RECORD_ID}" ]; then
    echo "Error: Missing environment variable CLOUDFLARE_RECORD_ID."
    exit 0
fi
if [ -z "${SCHEDULE_EVERY}" ]; then
    echo "Warning: Missing environment variable SCHEDULE_EVERY using default value 10 minutes."
fi

auth_key=${CLOUDFLARE_AUTH_KEY}
auth_email=${CLOUDFLARE_AUTH_EMAIL}
domain_name=${CLOUDFLARE_DOMAIN_NAME}
api=${CLOUDFLARE_API}
zone=${CLOUDFLARE_ZONE_ID}
record_id=${CLOUDFLARE_RECORD_ID}
schedule_every=${SCHEDULE_EVERY}

#######################################
# Update domain name with current public ip
# https://api.cloudflare.com/#dns-records-for-a-zone-dns-record-details
# Returns:
#   Cloudflare api response
#######################################
update_domain() {
	curl -X PUT "$api/zones/$zone/dns_records/$record_id" \
		-H "X-Auth-Email: $auth_email" \
		-H "X-Auth-Key: $auth_key" \
		-H "Content-Type: application/json" \
		--data '{"type":"A","name":"'${domain_name}'","content":'$1',"ttl":1,"proxied":false}'
} 

######################################
# Get public IP from dyndns
# Returns:
#   Public IP
#######################################
get_public_ip() {
	echo '"'$( curl -s checkip.dyndns.org \
	| sed -e 's/.*Current IP Address: //' -e 's/<.*$//')'"'
} 


while :
do
	public_ip=$( get_public_ip )
	echo $( update_domain ${public_ip} )
	sleep ${schedule_every} || sleep 6000
done
